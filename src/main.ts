import {Game} from "./models/Game";

const game = new Game();

/**
 * Help the Christmas elves fetch presents in a magical labyrinth!
 *
 */

// game loop
while (true) {
    // @ts-ignore
    const turnType = parseInt(readline(), 10);
    game.setTurnType(turnType);
    for (let i = 0; i < 7; i++) {
        // @ts-ignore
        const inputs = readline().split(' ');
        for (let j = 0; j < 7; j++) {
            const tile = inputs[j];
            game.setBoard(tile, i, j);
        }
    }
    for (let i = 0; i < 2; i++) {
        // @ts-ignore
        const inputs = readline().split(' ');
        const numPlayerCards = parseInt(inputs[0], 10); // the total number of quests for a player (hidden and revealed)
        const playerX = parseInt(inputs[1], 10);
        const playerY = parseInt(inputs[2], 10);
        const playerTile = inputs[3];
        if(i === 0) {
            //my player
            game.setPositionMyPlayer({x:playerX, y:playerY});
        }else{
            game.setPositionEnemy({x:playerX, y:playerY});
        }
    }
    // @ts-ignore
    const numItems = parseInt(readline(), 10); // the total number of items available on board and on player tiles
    for (let i = 0; i < numItems; i++) {
        // @ts-ignore
        const inputs = readline().split(' ');
        const itemName = inputs[0];
        const itemX = parseInt(inputs[1], 10);
        const itemY = parseInt(inputs[2], 10);
        const itemPlayerId = parseInt(inputs[3], 10);
        if(itemPlayerId === 0) {
            //my player
            game.setPositionTarget({x:itemX, y:itemY});
        }else{
            //enemy
        }
    }
    // @ts-ignore
    const numQuests = parseInt(readline(), 10); // the total number of revealed quests for both players
    for (let i = 0; i < numQuests; i++) {
        // @ts-ignore
        const inputs = readline().split(' ');
        const questItemName = inputs[0];
        const questPlayerId = parseInt(inputs[1], 10);
    }

    /*
     * Write an action using console.log()
     * To debug: console.error('Debug messages...');
     */

    // PUSH <id> <direction> | MOVE <direction> | PASS
    console.log(game.nextAction());
}
