import {Coord} from "./Coord";
import {Board} from "./Board";

export class Tile {
    public value: string;
    public coords: Coord;

    public canGoUp(): boolean {
        return this.value.charAt(0) === "1";
    }

    public canGoRight(): boolean {
        return this.value.charAt(1) === "1";
    }

    public canGoDown(): boolean {
        return this.value.charAt(2) === "1";
    }

    public canGoLeft(): boolean {
        return this.value.charAt(3) === "1";
    }

    public hasRightTile(board: Board): boolean {
        return (
            board.coords[this.coords.y] &&
            // eslint-disable-next-line no-undefined
            board.coords[this.coords.y][this.coords.x + 1] !== undefined
        );
    }

    public getRightTile(board: Board): Tile | null {
        return board.coords[this.coords.y][this.coords.x + 1];
    }

    public hasLeftTile(board: Board): boolean {
        return (
            board.coords[this.coords.y] &&
            // eslint-disable-next-line no-undefined
            board.coords[this.coords.y][this.coords.x - 1] !== undefined
        );
    }

    public getLeftTile(board: Board): Tile | null {
        return board.coords[this.coords.y][this.coords.x - 1];
    }

    public hasUpTile(board: Board): boolean {
        return (
            board.coords[this.coords.y - 1] &&
            // eslint-disable-next-line no-undefined
            board.coords[this.coords.y - 1][this.coords.x] !== undefined
        );
    }

    public getUpTile(board: Board): Tile | null {
        return board.coords[this.coords.y - 1][this.coords.x];
    }

    public hasDownTile(board: Board): boolean {
        return (
            board.coords[this.coords.y + 1] &&
            // eslint-disable-next-line no-undefined
            board.coords[this.coords.y + 1][this.coords.x] !== undefined
        );
    }

    public getDownTile(board: Board): Tile | null {
        return board.coords[this.coords.y + 1][this.coords.x];
    }
}
