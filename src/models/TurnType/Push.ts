import {Turn} from "./Turn";

export class Push extends Turn {
    public nextAction(): string {
        return this.canGoToTarget() ? this.attack() : this.defend();
    }

    private haveToPushVertically(): boolean {
        return this.positionMyPlayer.y === this.target.y;
    }

    private static addOne(number: number): number {
        return number > 5 ? 6 : number + 1;
    };

    private static subOne(number: number): number {
        return number < 1 ? 0 : number - 1;
    }

    private canGoToTarget(): boolean {
        if(this.targetIsRight() && this.canGoRight()) {
            return true;
        } else if(this.targetIsLeft() && this.canGoLeft()) {
            return true;
        } else if(this.targetIsUp() && this.canGoUp()) {
            return true;
        } else if(this.targetIsDown() && this.canGoDown()) {
            return true;
        }

        return false;
    }

    private defend(): string {
        if(this.haveToPushVertically()) {
            if(this.targetIsRight() && this.canGoRight()) {
                return `PUSH ${Push.addOne(this.positionMyPlayer.x)} UP`;
            }
            if(this.canGoLeft()) {
                return `PUSH ${Push.subOne(this.positionMyPlayer.x)} UP`;
            }
        }
        if(this.targetIsDown() && this.canGoDown()) {
            return `PUSH ${Push.addOne(this.positionMyPlayer.y)} RIGHT`;
        }
        if(this.canGoRight()) {
            return `PUSH ${Push.subOne(this.positionMyPlayer.y)} RIGHT`;
        }
        return `PUSH ${Push.addOne(this.positionMyPlayer.x)} UP`;
    }

    private attack(): string {
        return `PUSH ${this.positionEnemy.x} RIGHT`;
    }
}
