import {Turn} from "./Turn";

export class Move extends Turn {

    public nextAction(): string {
        if(this.targetIsRight() && this.canGoRight()) {
            return 'MOVE RIGHT';
        } else if(this.targetIsLeft() && this.canGoLeft()) {
            return 'MOVE LEFT';
        } else if(this.targetIsUp() && this.canGoUp()) {
            return 'MOVE UP';
        } else if(this.targetIsDown() && this.canGoDown()) {
            return 'MOVE DOWN';
        }
        return 'PASS';
    }

}
