import {Game} from "../Game";
import {Tile} from "../Tile";
import {Coord} from "../Coord";
import {Board} from "../Board";

export class Turn {
    protected game: Game;
    protected currentTile: Tile;
    protected positionMyPlayer: Coord;
    protected board: Board;
    protected target: Coord;
    protected positionEnemy: Coord;

    constructor(game) {
        this.game = game;
        this.board = this.game.board;
        this.target = this.game.target;
        this.currentTile = this.game.board.currentTile;
        this.positionMyPlayer = this.game.board.positionMyPlayer;
        this.positionEnemy = this.game.board.positionEnemy;
    }

    public targetIsRight(): boolean {
        return this.positionMyPlayer.x - this.game.target.x < 0;
    }

    public targetIsLeft(): boolean {
        return this.positionMyPlayer.x - this.game.target.x > 0;
    }

    public targetIsUp(): boolean {
        return this.positionMyPlayer.y - this.game.target.y > 0;
    }

    public targetIsDown(): boolean {
        return this.positionMyPlayer.y - this.game.target.y < 0;
    }

    public canGoRight(): boolean {
        return this.currentTile.canGoRight() && this.currentTile.hasRightTile(this.board) && this.currentTile.getRightTile(this.board).canGoLeft();
    }

    public canGoLeft(): boolean {
        return this.currentTile.canGoLeft() && this.currentTile.hasLeftTile(this.board) && this.currentTile.getLeftTile(this.board).canGoRight();
    }

    public canGoUp(): boolean {
        return this.currentTile.canGoUp() && this.currentTile.hasUpTile(this.board) && this.currentTile.getUpTile(this.board).canGoDown();
    }

    public canGoDown(): boolean {
        return this.currentTile.canGoDown() && this.currentTile.hasDownTile(this.board) && this.currentTile.getDownTile(this.board).canGoUp();
    }

}
