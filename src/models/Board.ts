import { Tile } from "./Tile";
import { Coord } from "./Coord";

export class Board {
    public coords: Array<Array<Tile>> = [];
    public currentTile: Tile;
    public positionMyPlayer: Coord;
    public positionEnemy: Coord;

    setCurrentTile(currentTile: Tile): void {
        this.currentTile = currentTile;
    }

    setPositionMyPlayer(positionMyPlayer: Coord): void {
        this.positionMyPlayer = positionMyPlayer;
    }

    setPositionEnemy(positionEnemy: Coord): void {
        this.positionEnemy = positionEnemy;
    }
}
