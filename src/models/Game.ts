import { Move } from "./TurnType/Move";
import { Push } from "./TurnType/Push";
import { Board } from "./Board";
import { Coord } from "./Coord";
import { Tile } from "./Tile";

export class Game {
    private turnType: number;
    public readonly board: Board;
    public target: Coord;

    public constructor() {
        this.board = new Board();
    }

    setTurnType(turnType: number): void {
        this.turnType = turnType;
    }

    setBoard(tileValue: string, line: number, position: number): void {
        const boardLine = this.board.coords[line];
        const coords = new Coord();
        coords.y = line;
        coords.x = position;
        const tile = new Tile();
        tile.value = tileValue;
        tile.coords = coords;
        if (boardLine) {
            this.board.coords[line][position] = tile;
        } else {
            const pos = [];
            pos[position] = tile;
            this.board.coords[line] = pos;
        }
    }

    setPositionMyPlayer(param: Coord): void {
        this.board.setPositionMyPlayer(param);
        this.board.setCurrentTile(this.board.coords[this.board.positionMyPlayer.y][this.board.positionMyPlayer.x]);
    }

    setPositionEnemy(param: Coord): void {
        this.board.setPositionEnemy(param);
    }

    setPositionTarget(param: Coord): void {
        this.target = param;
    }

    public nextAction(): string {
        let turn;
        if (this.turnType === 1) {
            turn = new Move(this);
        } else {
            turn = new Push(this);
        }
        return turn.nextAction();
    }
}
